from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tensorflow as tf

import modeling
import run_classifier
import tokenization
from make_training_data import get_data
from question_task import TaskProcessor


class BaseInit(object):
    def __init__(self):
        self.bert_model = 'uncased_L-12_H-768_A-12'
        self.bert_pretrained_dir = 'pre-trained_models/' + self.bert_model
        self.output_dir = 'models/'
        self.task_data_dir = 'data'
        self.training_data_file = 'training_data/train.csv'
        self.label_mapping = get_data(self.training_data_file, self.task_data_dir)
        # model hyper parameters
        self.train_batch_size = 16
        self.eval_batch_size = 2
        self.learning_rate = 2e-5
        self.num_train_epochs = 1.0
        self.warmup_proportion = 0.1
        self.max_seq_length = 32
        # model configs
        self.save_checkpoints_steps = 1000
        self.iterations_per_loop = 1000
        self.num_tpu_cores = None
        self.vocab_file = os.path.join(self.bert_pretrained_dir, 'vocab.txt')
        self.config_file = os.path.join(self.bert_pretrained_dir, 'bert_config.json')
        self.init_checkpoint = os.path.join(self.bert_pretrained_dir, 'bert_model.ckpt')
        self.do_lower_case = self.bert_model.startswith('uncased')

        self.processor = TaskProcessor()
        self.label_list = self.processor.get_labels()
        self.tokenizer = tokenization.FullTokenizer(vocab_file=self.vocab_file,
                                                    do_lower_case=self.do_lower_case)

        self.run_config = tf.contrib.tpu.RunConfig(
            model_dir=self.output_dir,
            save_checkpoints_steps=self.save_checkpoints_steps,
            tpu_config=tf.contrib.tpu.TPUConfig(
                iterations_per_loop=self.iterations_per_loop,
                num_shards=self.num_tpu_cores,
            ))

        self.train_examples = self.processor.get_train_examples(self.task_data_dir)
        self.num_train_steps = int(
            len(self.train_examples) / self.train_batch_size * self.num_train_epochs)
        self.num_warmup_steps = int(self.num_train_steps * self.warmup_proportion)

        self.model_fn = run_classifier.model_fn_builder(
            bert_config=modeling.BertConfig.from_json_file(self.config_file),
            num_labels=len(self.label_list),
            init_checkpoint=self.init_checkpoint,
            learning_rate=self.learning_rate,
            num_train_steps=self.num_train_steps,
            num_warmup_steps=self.num_warmup_steps,
            use_tpu=False,
            use_one_hot_embeddings=True)

        self.estimator = tf.contrib.tpu.TPUEstimator(
            use_tpu=False,
            model_fn=self.model_fn,
            config=self.run_config,
            train_batch_size=self.train_batch_size,
            eval_batch_size=self.eval_batch_size,
            predict_batch_size=1)