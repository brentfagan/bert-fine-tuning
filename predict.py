from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import tensorflow as tf
import sys
import tokenization
import numpy as np

import run_classifier
from baseinit import BaseInit


class Predict(BaseInit):
    def __init__(self):
        BaseInit.__init__(self)

    def _create_examples(self, text_a, text_b, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        guid = "%s-%s" % (set_type, 1)
        text_a = tokenization.convert_to_unicode(text_a)
        text_b = tokenization.convert_to_unicode(text_b)
        if set_type == "test":
            label = "0"
        examples.append(
            run_classifier.InputExample(guid=guid, text_a=text_a, text_b=text_b, label=label))
        return examples

    def predict(self, question, answer):
        pred_examples = self._create_examples(question, answer, 'test')
        pred_features = run_classifier.convert_examples_to_features(
            pred_examples, self.label_list, self.max_seq_length, self.tokenizer)
        pred_input_fn = run_classifier.input_fn_builder(
            features=pred_features,
            seq_length=self.max_seq_length,
            is_training=False,
            drop_remainder=True)
        predictions = self.estimator.predict(input_fn=pred_input_fn)
        for pred in predictions:
            print('class label: {}'.format(self.label_mapping[np.argmax(pred)]))


if __name__ == '__main__':
    question = sys.argv[1]
    answer = sys.argv[2]
    p = Predict()
    p.predict(question, answer)