# Notes

Some notes for implementing fine tuning on `uncased_L-12_H-768_A-12` pre-trained model.

* `self.bert_model`, `self.bert_pretrained_dir`, and `self.training_data_file` are all hard-coded in `baseinit.py`
* `NUM_CLASSES` is hard-coded as a global varialbe in `question_task.py`
* `train.py`, `evaluate.py`, and `predict.py` are the only other altered/new files.
* `predict.py` takes a survey question, answer pair and categorizes it into one of 450 possible topics