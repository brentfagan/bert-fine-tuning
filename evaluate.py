from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import os
import tensorflow as tf

import run_classifier
from baseinit import BaseInit


class Evaluate(BaseInit):
    def __init__(self):
        BaseInit.__init__(self)

    def evaluate(self):
        eval_examples = self.processor.get_dev_examples(self.task_data_dir)
        eval_features = run_classifier.convert_examples_to_features(
            eval_examples, self.label_list, self.max_seq_length, self.tokenizer)
        print('***** Started evaluation at {} *****'.format(datetime.datetime.now()))
        print('  Num examples = {}'.format(len(eval_examples)))
        print('  Batch size = {}'.format(self.eval_batch_size))
        # Eval will be slightly WRONG on the TPU because it will truncate
        # the last batch.
        eval_steps = int(len(eval_examples) / self.eval_batch_size)
        eval_input_fn = run_classifier.input_fn_builder(
            features=eval_features,
            seq_length=self.max_seq_length,
            is_training=False,
            drop_remainder=True)
        result = self.estimator.evaluate(input_fn=eval_input_fn, steps=eval_steps)
        print('***** Finished evaluation at {} *****'.format(datetime.datetime.now()))
        output_eval_file = os.path.join(self.output_dir, "eval_results.txt")
        with tf.gfile.GFile(output_eval_file, "w") as writer:
          print("***** Eval results *****")
          for key in sorted(result.keys()):
            print('  {} = {}'.format(key, str(result[key])))
            writer.write("%s = %s\n" % (key, str(result[key])))

if __name__ == '__main__':
    e = Evaluate()
    e.evaluate()