from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import tensorflow as tf

import run_classifier
from baseinit import BaseInit


class Train(BaseInit):
    def __init__(self):
        BaseInit.__init__(self)

    def train(self):
        print('Training on 70% of training data takes roughly 20 minutes. Please wait...')
        train_features = run_classifier.convert_examples_to_features(
            self.train_examples, self.label_list, self.max_seq_length, self.tokenizer)
        print('***** Started training at {} *****'.format(datetime.datetime.now()))
        print('  Num examples = {}'.format(len(self.train_examples)))
        print('  Batch size = {}'.format(self.train_batch_size))
        # tf.logging.info("  Num steps = %d", self.num_train_steps)
        train_input_fn = run_classifier.input_fn_builder(
            features=train_features,
            seq_length=self.max_seq_length,
            is_training=True,
            drop_remainder=True)
        self.estimator.train(input_fn=train_input_fn, max_steps=self.num_train_steps)
        print('***** Finished training at {} *****'.format(datetime.datetime.now()))

if __name__ == '__main__':
    t = Train()
    t.train()
